# Lab: EDA - Schematic Capture

## Setup Git Repository
1. Create a new KiCad project called `voltage_regulated_led`.
2. Using your Terminal or Git Bash, navigate to the directory of your KiCad project.
3. Initialize a new Git repository in this directory.
4. Create a new file called `README.md` and copy the contents of this file into it.
5. Add a `.gitignore` file to your repository with the following content:
```plaintext
# Adapted from: https://github.com/github/gitignore/blob/main/KiCad.gitignore

# Temporary files
*.000
*.bak
*.bck
*.kicad_pcb-bak
*.kicad_sch-bak
*-backups
*.kicad_prl
*.sch-bak
*~
_autosave-*
*.tmp
*-save.pro
*-save.kicad_pcb
fp-info-cache

# Netlist files (exported from Eeschema)
*.net

# Autorouter files (exported from Pcbnew)
*.dsn
*.ses
```

## Recreate Schematic 
1. Create a new project and re-create the schematic shown below.

![](schematic.png)

2. Be sure to complete the complete the document metadata to populate the footer (and add a fun logo).
3. Make sure that your schematic passes ERC. Save and commit an `ERC.rpt` file with no warnings / errors to your repository.
4. Generate a CSV of the Bill of Materials for all of the components; commit this to your repository.
5. Generate a PDF of your schematic (`File` $\rightarrow$ `Plot...`) call `voltage_regulated_led_schematic.pdf`; commit this to your repository.
6. Set the `Rev #` in the document metadata to `v1.0.0`.
7. Create a tag of this schematic called `v1.0.0`.

## Modify Schematic: Connectors and Sheets 
1. Move the `Push Button Activated LED` circuit to a new sheet.
2. Make `BT1`, `D2`, `SW1` and `Sw2` be panel-mounted components that share a 1x8 header pin connector on the PCB, with each peripheral component connecting using a 1x2 pin socket connector.
3. Like above, generate a BOM CSV file, `ERC.rpt` file, and PDF of your modified schematic sheets, overwriting the previous files in your repository (remember, they are saved in the git commit history).
4. Update the `Rev #` in the document metadata to `v2.0.0`.
5. Create a new annotated tag of this modified schematic called `v2.0.0`.

## Layout a Single-Sided PCB
Next you will create a PCB from the schematic that you captured last week.

1. Assign footprints to all parts in your schematic.
    -   All resistors and capacitors and other passive components should be THT. 
    -   Keep in mind that `C1` and `C2`, are *polarized* THT.
    -   Your voltage regulator (`U1:L7805`) should use a THT `TO220` package.
    -   Your pins and sockets should be 2.54 mm vertical orientation (`Pin Socket:1x02,P2.54mm,Vertical`) to allow external wires to be attached to your PCB.
2. Set your **Design Rules**:
    -   Trace width $\geq$ 20 mil
    -   Clearance $\geq$ 32 mil
    -   Drill diameter $\geq$ 1 mm
3. Set your **Page Settings** for the sheet footer metadata.
4. Your board should be single-sided.
5. Optimize the size of your board to be as small as possible in your layout (as outlined with `Edge.Cuts`).
6. Place mounting holes on each corner of the board (add to the schematic), no closer than 2 mm to any edge.
7. Remember that THT component bodies should be on the *opposite* side of the board from their traces (in contrast to SMD components).
8. Use filled ground pours where possible.
9. Make sure that your schematic passes the **Design Rules Check (DRC)**.  Include a DRC output file in your repository.
10. Generate a PDF of your PCB layout (`File` $\rightarrow$ `Plot...`) call `voltage_regulated_led_pcb.pdf`; commit this to your repository.
11. Set the `Rev #` in the PCB document metadata to `v2.1.0`.
12. Create a tag of this PCB layout called `v2.1.0`.

## What to Submit
* Upload the PDF to the corresponding Gradescope assignment.
* Make sure that all of your local git repository commits--including your annotated tags--have been pushed to GitLab.
* Make sure that you add `mlp6` and `drm66` as Maintainers to your GitLab repository. 