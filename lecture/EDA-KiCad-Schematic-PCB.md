# Electronics Design Automation: Schematic Capture & PCB Layout

## Overview
![](kicad_logo_small.png)

* Certain video cards will not work well with the PCB tool.  In that case, disable the `Accelerated` toolset and choose the `Fallback` graphics toolset.
* On Macs, your part / footprint / model libraries might not be detected automatically after installation.  You might need to manually point Kicad at them in a path similar to: `Macintosh/Library/Application Support/kicad/templates/sym-lib-table/`.
* Documentation: https://docs.kicad.org/7.0/en/getting_started_in_kicad/getting_started_in_kicad.html

##  Schematics
### UI/UX

![](commands_overview.png)

https://docs.kicad.org/7.0/en/eeschema/eeschema.html#schematic-editing-operations

### Schematic Capture
1. Create a `New Project`, which creates a schematic file.
2. **DO NOT CHANGE THE GRID SPACING!!**
3. Setup `Page Settings...`, using [semantic versionin](https://semver.org/) for `Revision`.
4. Configure `Schematic Setup...`, including `Project:Net Classes`.
5.  Place component / part (`Place Symbol`) using default library. If component doesn't exist, then you either need to:
    -   Download the part from an online database (e.g.,
        [SnapEDA](https://www.snapeda.com/))
    -   Create part using the `Library Editor`.

![](symbols-labeled.png)

6.  Annotate components (give each component a unique label).  *This is now done automatically in KiCad 7.x, but you may want to manually override some of the annotation defaults.*
7.  Assign component values
8.  Label nets with meaningful names
    -   Nets are like nodes; common voltage connections.
    -   Use Global net labels to avoid connection chaos.

### Power Ports
* Power ports, including ground references, are also global net labels.
* Component pins can be explicitly designated power in/out (in contrast to signal).
* Power, ideally, cascades top-to-bottom (`+` $\rightarrow$ `GND` \[$\rightarrow$ `-`\]) on the schematic.
![](symbols-pwr-flag.png)

### Electrical Rules Check (ERC)
* Check the validity of the schematic
* Common error message:

> "Input Power pin not driven by any Output Power pins"

* KiCad checks to make sure that power can "drive" components that demand that input.
* You can explicitly indicate this in the schematic using the `PWR_FLAG` symbol attached to the net in question.
* Might need to re-map pin types (`Properties:Edit Symbol:Pin Table`).

![](symbols-pwr-flag.png)

### Best Practices
* Signal, ideally, flows left-to-right (input $\rightarrow$ output).
* Outline and label functional blocks.  Use `Heirarchical Sheets` to organize more clearly-defined sub-circuits.
* Can include non-electrical items, like `Mounting Holes`.
* Add Test Pins/Pads to nets you will want to verify during testing.
    - Power nets
    - Signal I/O
* Use `No-Connection` flags for pins that are intentionally not connected to other components.

![](schematic_annotated.png)

## In-Class Exercise (Part I)
* [ ] Create a simple LED powered by a battery.

### Schematic Components Not on the PCB
If a component will not exist on the PCB:
* Be sure to exclude it from the board (`Properties` $\rightarrow$ `General` $\rightarrow$ **Exclude from Board**).
* Represent the **Connectors** or **Terminal Blocks** on th schematic that will be used to connect the component to the PCB by wires.

### In-Class Exercise (Part II)
Modify your circuit to:
* [ ] Specify using a CR2032 coin cell battery that will be mounted on the PCB.
* [ ] Connect the LED off the PCB using a connector.

### Heirarchecal Sheets
* Circuits can get very complicated very quickly; quickly separated from functional signal flow.
* Can organize functional sub-circuits into **sheets** (similar to writing functions in software development).
* Sheets require explicit types of "pins" to allow signals to communicate between sheets.
* Power ports are **global** and do not need to be explicitly connects.

### Example Project Schematic
![](065-0100_Main_Board-1.png)
![](065-0100_Main_Board-5.png)
![](065-0100_Main_Board-6.png)
![](065-0100_Main_Board-3.png)

### Bill of Materials (BOM)
* All of the components in the schematic can be populated into a Bill of Materials.
* Industry-standard ECAD packages will be automatically linked to online part catalogs to order.
* Specific components can be excluded from the BOM (`Properties`).

## PCB Layout

### Why a PCB?
* Breadboards are good for simple testing, but not robust.
* Need to reduce form factor.
* Reduce noise, capacitive effects, etc.
* SMD (versus thru-hole) components are becoming ubiquitous.
![](breadboard.jpg)

### PCB Examples
![](pcb_rev1.png)
![](pcb_layout_rev1.png)
![](pcb_layout_rev1_bottom.png)

### Two-layer PCB
* Single-layer boards for "trivial" layouts.
* Two-layer board best balance of flexibility and complexity for "simple" layouts.
* $>$ 2 layers increases complexity (but putting power and ground on their own layers can be advantageous)

![](pcb_layers_annotated.png)

### THT vs. SMD
* Surface mount components on the same side as the traces
* Soldering using reflow.
* Thru-hole components on the opposite side from the traces (ease of soldering).

![](tht_component.png)
![](smd_components.jpg)

### PCB Editor
![](pcbnew_ui.png)

### Laying out a PCB
1. Make sure that all parts are **annotated** and have **footprints**.
2. Create PCB from schematic
3. All component pins/pads that need to be connected with traces will be connected with airwires.
4. Use **Graphics Lines** tool to create board outline (Layer: `Edge.Cuts`)
5. Layout components based on your design needs / constraints, considering orientation, board side, etc.
6. Setup **Design Rules** (`File` $\rightarrow$ `Board Setup...`):
    - Trace width ($\geq$ 20 mil)
    - Drill diameter ($\geq$ 1 mm)
    - Clearance ($\geq$ 32 mil)
7. Setup **Net Classes** for signal and power/ground (wider / more clearance).
8.  Route traces (Make sure that you are on the correct layer!)
9.  Use `Filled Zone` tool to create a copper pour (`F/B.Cu`), usually associated with the `GND` net.
10. Clearout other zones, as needed.
11. Perform **Design Rule Check (DRC)**

### Power Traces / Layers
* Larger trace width for greater power delivery (less resistance for more current).
* Even better to dedicate an entire layer
    -   More direct return paths
    -   Shielding (EMI)
    -   Reduce noise (switching)
    -   Dissipate heat
### Copper Ground Pours
* Help reject noise \& interference
* Lower impedance to ground
* Save milling time and bits!!
* Be sure not to have any isolated `GND` islands.
![](pwr_vs_signal_traces.jpg)

### Reducing Noise
* Separate analog (`AGND`) and digital grounds (`GND`), and electrically connect using a **Net Tie**.
* Reduce trace "loops" (RF interference)
* Place decoupling capacitors next to power pins they are supporting.

### So many types of ground connections...
![](grounds01.png)
![](grounds02.png)
https://electronics.stackexchange.com/questions/392911/kicad-5-what-is-the-significance-of-the-various-gnd-symbols

### Tips-n-Tricks
* You need to make sure that you update your PCB everytime you edit your schematic.  Failing to do so will create asyncrony of the two documents, which will be **painful** to resolve.
* Clean up airwires with the `Ratsnest` tool.
* **Vias**: connections between top and bottom layers of a board.  These can be plated or connected with a physical wire.
* Use plugs/sockets for wire connections.
* Strategically place test pins / pads.
* Define `clearout` zones for areas where components cannot be placed (e.g., RF antenna, plug body)
* Parallel running connections (e.g., power & ground) can be routed as a **Differential Pair**.

### Thermal Relief
![](thermal_relief.jpg)


### PCB Examples (again)

![](pcb_layout_rev1.png)

![](pcb_layout_rev1_bottom.png)

![](pcb_rev1.png)

## Resources
* [PCB Basics (SparkFun)](https://learn.sparkfun.com/tutorials/pcb-basics)
* [KiCad Docs: PCB Editor](https://docs.kicad.org/7.0/en/pcbnew/pcbnew.html)
* [Example KiCad Project](https://gitlab.oit.duke.edu/MedTechPrototypingSkills/kicad_test)
* [Grounding and Voltage Routing Considerations in PCB Design](https://resources.pcb.cadence.com/blog/2020-grounding-and-voltage-routing-considerations-in-pcb-design-e2ek)
* [Effective PCB Trace Width and Spacing](https://resources.pcb.cadence.com/blog/2021-effective-pcb-trace-width-and-spacing)